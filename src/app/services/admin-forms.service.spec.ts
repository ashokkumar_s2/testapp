import { TestBed } from '@angular/core/testing';

import { AdminFormsService } from './admin-forms.service';

describe('AdminFormsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AdminFormsService = TestBed.get(AdminFormsService);
    expect(service).toBeTruthy();
  });
});
