import { Component, OnInit } from '@angular/core';
import { FormBuilder,FormGroup, Validators } from '@angular/forms';
import { UsersService } from '../../services/users.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private router: Router,private formBuilder: FormBuilder, private usersService: UsersService) { }

    
  loginForm:FormGroup;
  submitted:boolean = false;
  messagePanel: string = "";
  messageFlag: boolean = false;

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      userName:['',[Validators.required]],
      password:['',[Validators.required]]
    });
  }

  get f() { return this.loginForm.controls; }

  validationRemover(){
    this.messageFlag = false;
  }

  onSubmit() {

    if(this.loginForm.valid){
     this.usersService.login(this.loginForm.value.userName, this.loginForm.value.password).subscribe(res=>{
      if(res.status == 200){
        let data:{} = res.content[0];
        if(data['role'] == 2){
          this.router.navigate(['app/admin/dashboard']);
        }
        else if(data['role'] == 1 || data['role'] == 3){
          this.router.navigate(['app/teacher/dashboard']);
        }
        else if(data['role'] == 5){
          this.router.navigate(['app/management/dashboard']);
        }
        else{
          this.router.navigate(['app/student/dashboard']);
        }
      }
      else{
        this.messageFlag = true;
        this.messagePanel = res.message + ":(";
      }
     })
    }
    else{
     this.messageFlag = true;
     this.messagePanel = "User name and password is required";
    }
 }

  }
 
