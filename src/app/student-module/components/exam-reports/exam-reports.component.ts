import { Component, OnInit } from '@angular/core';
import { StudentService} from '../../../services/student.service';

@Component({
  selector: 'app-exam-reports',
  templateUrl: './exam-reports.component.html',
  styleUrls: ['./exam-reports.component.scss']
})
export class ExamReportsComponent implements OnInit {

  reports:any[] = [];

  constructor(private studentService:StudentService) { }

  ngOnInit() {
    this.studentService.getPublishedReports().subscribe(res=>{
      if (res.status == 200) {
        this.reports = res.content;
      }
      else{
        //error flag
      }
    })
  }

}
