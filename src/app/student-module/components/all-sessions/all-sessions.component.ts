import { Component, OnInit,ViewChild } from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { StudentService} from '../../../services/student.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-all-sessions',
  templateUrl: './all-sessions.component.html',
  styleUrls: ['./all-sessions.component.scss']
})
export class AllSessionsComponent implements OnInit {

  displayedColumns: string[] = ['Date', 'Session','Schedule', 'Subject', 'Teacher','Status'];
  dataSource :any;
  constructor(private studentService:StudentService,private router:Router) { }

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  ngOnInit() {
    this.studentService.getAllSessions().subscribe(res=>{
     this.dataSource = new MatTableDataSource(res.content);
     this.dataSource.paginator = this.paginator;
     this.dataSource.sort = this.sort;
    })
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
