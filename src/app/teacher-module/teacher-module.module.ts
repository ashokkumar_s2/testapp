import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectizeModule } from 'ng-selectize';
import { HttpClientModule } from '@angular/common/http';
import { AuthGuard } from '../auth.guard';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { ToastrModule } from 'ngx-toastr';
import {MatTableModule} from '@angular/material/table';
import {MatSortModule} from '@angular/material/sort';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import { NgbDatepickerModule} from '@ng-bootstrap/ng-bootstrap';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { Ng2OdometerModule } from 'ng2-odometer';

import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ManageStudentsComponent } from './components/manage-students/manage-students.component';
import { CreateStudentsComponent } from './components/create-students/create-students.component';
import { ManageScheduleComponent } from './components/manage-schedule/manage-schedule.component';
import { CreateScheduleComponent } from './components/create-schedule/create-schedule.component';
import { ScheduleDescriptionComponent } from './components/schedule-description/schedule-description.component';
import { SchedulePanelComponent } from './components/schedule-panel/schedule-panel.component';
import { MyapprovalsComponent } from './components/myapprovals/myapprovals.component';
import { SwapdetailedComponent } from './components/swapdetailed/swapdetailed.component';
import { MyschedulesComponent } from './components/myschedules/myschedules.component';
import { ExamsandtestComponent } from './components/examsandtest/examsandtest.component';
import { ExamreportDetailedComponent } from './components/examreport-detailed/examreport-detailed.component';
import { ExamReportConfigurationComponent } from './components/exam-report-configuration/exam-report-configuration.component';
import { ConfigDetailedComponent } from './components/config-detailed/config-detailed.component';
import { TeacherExamReportComponent } from './components/teacher-exam-report/teacher-exam-report.component';
import { ManageTeachersComponent } from './components/manage-teachers/manage-teachers.component';
import { SwapTeachersComponent } from './components/swap-teachers/swap-teachers.component';
import { TeacherDetailComponent } from './components/teacher-detail/teacher-detail.component';
import { TeacherProfileComponent } from './components/teacher-profile/teacher-profile.component';
import { TeacherAttendanceComponent } from './components/teacher-attendance/teacher-attendance.component';
import { TeacherReportComponent } from './components/teacher-report/teacher-report.component';


const routes: Routes = [
  { path: 'dashboard' , component: DashboardComponent, canActivate:[AuthGuard], data: {roles: [1,3]}},
  { path: 'managestudents', component: ManageStudentsComponent,canActivate:[AuthGuard], data: {roles: [1,3]}},
  { path: 'createstudents/:id', component: CreateStudentsComponent ,canActivate:[AuthGuard], data: {roles: [1,3]}},
  { path: 'manageschedule', component: ManageScheduleComponent,canActivate:[AuthGuard], data: {roles: [1,3]}},
  { path: 'createschedule/:id', component: CreateScheduleComponent ,canActivate:[AuthGuard], data: {roles: [1,3]}},
  { path: 'scheduledescription/:id', component:ScheduleDescriptionComponent,canActivate:[AuthGuard], data: {roles: [1,3]}},
  { path: 'swapdetailed', component:SwapdetailedComponent,canActivate:[AuthGuard], data: {roles: [1,3]}},
  { path: 'myschedules', component:MyschedulesComponent,canActivate:[AuthGuard], data: {roles: [1,3]}},
  { path: 'examsandtest', component:ExamsandtestComponent,canActivate:[AuthGuard], data: {roles: [1,3]}},
  { path: 'examreportconf', component:ExamReportConfigurationComponent,canActivate:[AuthGuard], data: {roles: [1,3]}},
  { path: 'examconf/:id/:id2', component:ConfigDetailedComponent,canActivate:[AuthGuard], data: {roles: [1,3]}},
  { path: 'classexamreport/:id/:id2', component:TeacherExamReportComponent,canActivate:[AuthGuard], data: {roles: [1,3]}},
  { path: 'examreport/:id', component:ExamreportDetailedComponent,canActivate:[AuthGuard], data: {roles: [1,3]}},
  { path: 'manageteachers', component:ManageTeachersComponent,canActivate:[AuthGuard], data: {roles: [3]}},
  {
    path: 'teacherdetail/:id',
    component: TeacherDetailComponent,
    children: [
      { path: 'profile', component: TeacherProfileComponent, canActivate: [AuthGuard], data: { roles: [3] } },
      { path: 'attendance', component: TeacherAttendanceComponent, canActivate: [AuthGuard], data: { roles: [3] } },
      { path: 'examreport', component: TeacherReportComponent, canActivate: [AuthGuard], data: { roles: [3] } },
      { path: '', redirectTo: 'profile', pathMatch: 'full' }
    ]
  },
  { path: 'swapteachers', component:SwapTeachersComponent,canActivate:[AuthGuard], data: {roles: [3]}},
  { path: '', redirectTo:'dashboard',pathMatch:'full' }
];

@NgModule({
  declarations: [
    DashboardComponent,
    ManageScheduleComponent,
    CreateScheduleComponent,
    CreateStudentsComponent,
    ManageStudentsComponent,
    ScheduleDescriptionComponent,
    SchedulePanelComponent,
    MyapprovalsComponent,
    SwapdetailedComponent,
    MyschedulesComponent,
    ExamsandtestComponent,
    ExamreportDetailedComponent,
    ExamReportConfigurationComponent,
    ConfigDetailedComponent,
    TeacherExamReportComponent,
    ManageTeachersComponent,
    SwapTeachersComponent,
    TeacherDetailComponent,
    TeacherProfileComponent,
    TeacherAttendanceComponent,
    TeacherReportComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectizeModule,
    NgbModule,
    HttpClientModule,
    OwlDateTimeModule,
    NgbDatepickerModule,
    MatTableModule,
    MatSortModule,
    MatInputModule,
    MatPaginatorModule,
    OwlNativeDateTimeModule,
    ToastrModule.forRoot({
      timeOut: 1000,
      positionClass: 'toast-center-center'
    }),
    Ng2OdometerModule.forRoot(),
    RouterModule.forChild(routes)
  ]
})
export class TeacherModuleModule { }
