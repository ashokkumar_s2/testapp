import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder,FormGroupDirective, FormGroup, Validators,NgForm,FormArray } from '@angular/forms';
import { TeacherService } from '../../../services/teacher.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-schedule-description',
  templateUrl: './schedule-description.component.html',
  styleUrls: ['./schedule-description.component.scss']
})
export class ScheduleDescriptionComponent implements OnInit {

  sessionId: number;
  sessionDetail: any = {};
  errorFlag: boolean;
  daysName: any[] = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
  day: string = "";
  actionTypeOptions: any[] = [];
  studentOptions: any[] = [];
  groupOptions: any[] = [];
  absenteesList: any[] = [];
  absentees: any[] = [];
  prefilledFlag : boolean = false;
  swapInfo:any;
  studentConfig = {
    highlight: false,
    create: false,
    persist: true,
    plugins: ['dropdown_direction', 'remove_button'],
    dropdownDirection: 'down',
    labelField: 'fullname',
    valueField: 'student_fk',
    maxItems: 60
  }
  hideSubmitFromSwap:boolean = false;
  hideSubmitFromAbort:boolean = false;
  sessionForm: FormGroup;
  submitted: boolean = false;
  messagePanel: string;


  sessionConfig = {
    highlight: false,
    create: false,
    persist: true,
    plugins: ['dropdown_direction', 'remove_button'],
    dropdownDirection: 'down',
    labelField: 'period_name',
    valueField: 'period_id',
    maxItems: 12
  }

  constructor(private formBuilder: FormBuilder,private router:Router, private toastr: ToastrService,private teacherService: TeacherService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.sessionId = + this.route.snapshot.paramMap.get('id');

    this.sessionForm = this.formSettings();

    this.teacherService.getSessionInfo(this.sessionId).subscribe(res => {
      if (res.status = 200) {
        this.sessionDetail = res.content[0];
        let dayCalc = new Date(this.sessionDetail.date);
        this.day = this.daysName[dayCalc.getDay()];
        switch(this.sessionDetail.status_flag){
          case 1:
            this.teacherService.getSessionAbsentees(this.sessionId).subscribe(res=>{
              this.absenteesList = res.content;
              console.log(this.absenteesList);
              this.absenteesList.forEach(data=>{
                this.absentees.push(data['abs_fk'])
                console.log('trigger');
              })
            })

            this.fetchStudents().then(()=>{
              this.prefilledFlag = true;
              this.f.actionType.setValue(1);
              this.f.absentees.setValue(this.absentees);
              this.f.sessionDescription.setValue(this.sessionDetail.session_info);
              this.f.homeWork.setValue(this.sessionDetail.session_homework);
              this.sessionForm.updateValueAndValidity();
            });
            break;
          
          case 2:
            this.prefilledFlag = true;
            this.teacherService.getSwapInfo(this.sessionId).subscribe(res=>{
              let result:any[] = res.content;
              let index = result.findIndex(arr => arr.swap_status_fk == 1);
              this.swapInfo = res.content[index];
              this.getSwapGroupOptions().then(()=>{
                this.hideSubmitFromSwap = true;
                this.f.actionType.setValue(2);
                this.f.swapTo.setValue(this.swapInfo.swap_to);
                this.sessionForm.updateValueAndValidity();
              })
            })
            break;

          case 3:
          this.prefilledFlag = true;
          this.f.actionType.setValue(3);
          this.f.reasonToAbort.setValue(this.sessionDetail.abort_reason);
          this.sessionForm.updateValueAndValidity();
          this.hideSubmitFromAbort = true;
            break;

          default:
        }
      }
      else {
        this.errorFlag = false;
      }
    })



    this.teacherService.getSessionAction().subscribe(res => {
      if (res.status = 200) {
        this.actionTypeOptions = res.content;
      }
      else {
        this.errorFlag = false;
      }
    })
  }

  formSettings(){
   return  this.formBuilder.group({
      actionType: ['', [Validators.required]],
      absentees: [],
      sessionDescription: [''],
      homeWork: [''],
      allPresent: [0],
      noHomeWork: [0],
      swapTo:[''],
      reasonToAbort:['']
    });
  }

  get f() { return this.sessionForm.controls; }

  async fetchStudents(){
    this.teacherService.getSessionStudents(this.sessionId).subscribe(res => {
      if (res.status = 200) {
        this.studentOptions = res.content;
        return;
      }
      else {
        this.errorFlag = false;
        return;
      }
    })
  }
  actionChangeValidation() {
    console.log(this.f.actionType.value);
    switch (this.f.actionType.value) {
      //if session is happened in a expected way
      case "1":
        //service to get list of students
        if(this.studentOptions.length<1){
          this.fetchStudents();
        }
        //setting up form
        this.sessionForm.reset(this.formSettings().value);
        this.removeValidators(this.sessionForm);
        this.f.actionType.setValue(1);
        this.f.absentees.setValidators([Validators.required]);
        this.f.sessionDescription.setValidators([Validators.required]);
        this.f.homeWork.setValidators([Validators.required]);
        this.changeAbsenteesValidation()
        break;

      //if session is opted for swap
      case "2":
        //service to get list of groups
        if(this.groupOptions.length<1){
          this.getSwapGroupOptions();
        }
         
        //setting up form
        this.sessionForm.reset(this.formSettings().value)
        this.removeValidators(this.sessionForm);
        this.f.actionType.setValue(2);
        this.f.swapTo.setValidators([Validators.required]);
        break;

      //if session is opted for abort
      case "3":
        
      //setting up form
      this.sessionForm.reset(this.formSettings().value)
      this.removeValidators(this.sessionForm);
      this.f.actionType.setValue(3);
      this.f.reasonToAbort.setValidators([Validators.required]);
      break;

      default:
        console.log("Not matched");

    }


  }

  async getSwapGroupOptions(){
    this.teacherService.getGroupsofClass(this.sessionDetail.class_id).subscribe(res => {
      let uniqueGroups:any[] = [];
      if (res.status = 200) {
        let groups: any[] = res.content;
        // //find and remove the users own group to avoid swap to himself
        // let ownerIndex = [...new Set(groups.map(group => { return group['group_id'] || 0 }))].indexOf(this.sessionDetail.group_id);
        // console.log(ownerIndex);
        // console.log(groups);
        // groups.splice(ownerIndex, 1);
        //get only unique options
        groups = groups.filter(group=>{
          return group['group_id'] != this.sessionDetail.group_id
        });
        const map = new Map();
        for (const group of groups) {
          if (!map.has(group['group_id'])) {
            map.set(group['group_id'], true);    // set any value to Map
            uniqueGroups.push({
              group_id: group['group_id'],
              group_name: group['group_name']
            });
          }
        }
      }
      else {
        this.errorFlag = false;
      }
      this.groupOptions = uniqueGroups;
      return;
    })
  }


  changeAbsenteesValidation() {
    console.log('flag',this.f.allPresent.value);
    if (this.f.allPresent.value) {
      this.f.absentees.setValue('');
      this.f.absentees.clearValidators();
      this.f.absentees.updateValueAndValidity();
    }
    else {
      this.f.absentees.setValidators([Validators.required]);
      this.f.absentees.updateValueAndValidity();
    }

    if (this.f.noHomeWork.value) {
      this.f.homeWork.disable();
      this.f.homeWork.setValue('');
      this.f.homeWork.clearValidators();
      this.f.homeWork.updateValueAndValidity();
    }
    else {
      this.f.homeWork.setValidators([Validators.required]);
      this.f.homeWork.enable();
      this.f.homeWork.updateValueAndValidity();
    }
    console.log(this.sessionForm.controls);
  }

  submitFlag() {
    this.submitted = true;
  }

  onSubmit() {
    console.log(this.sessionForm.valid);
    if(this.sessionForm.valid){
      let postData = this.sessionForm.value;
      let abscount:[] = this.sessionForm.controls.absentees.value
      if(abscount){
        postData.abs_count = abscount.length;
      }
      else{
        postData.abs_count = 0;
      }
      postData.class_id = this.sessionDetail.class_id;
      postData.session_id =  this.sessionDetail.session_id;
      postData.group_id = this.sessionDetail.group_id;
      postData.subject_teacher_fk = this.sessionDetail.subject_teacher_fk;
      postData.date = this.sessionDetail.date;
      postData.period_id = this.sessionDetail.period_id;

      this.teacherService.postSessionData(postData).subscribe(res=>{
        if(res.status == 200){
          this.toastr.info('Hoorayy all set!',res.message);
        }
        else{
          this.toastr.warning('Contact Admin',res.message);
        }
      })
      this.redirect();
    }
  }

  removeValidators(form: FormGroup) {
    for (const key in form.controls) {
         form.get(key).clearValidators();
         form.get(key).updateValueAndValidity();
    }
}

cancelSwap(){
  if(this.swapInfo){
    let data = {
      swap_status:4, //4 is status flag for cancellation of swap by owner
      swap_id:this.swapInfo.swap_id,
      status_flag:null,
      session_id:this.swapInfo.session_fk,
      date: this.sessionDetail.date,
      period_id : this.sessionDetail.period_id,
      class_id : this.sessionDetail.class_id
    }
    this.teacherService.swapUpdate(data).subscribe(res=>{
      if(res.status == 200){
        this.toastr.info('shabashhhh....!!!!!',res.message);
      }
      else{
        this.toastr.warning('yuckkkkkk......!!!',res.message)
      }
    })
  }
  else{
    this.errorFlag = false;
    this.messagePanel = "There is no swap information linked to this session on backend. If it is unsolved contact admin team with error code";
  }
}

cancelAbort(){
  this.teacherService.cancelAbortRequest(this.sessionId).subscribe(res=>{
    if(res.status == 200){
      this.toastr.info('Woohoooooo.......!!!!!',res.message);
    }
    else{
      this.toastr.warning('yuckkkkkk......!!!',res.message);
    }
  })
}

redirect(){
  this.router.navigate(['app/dashboard']);
}

}
