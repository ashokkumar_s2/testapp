import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeacherExamReportComponent } from './teacher-exam-report.component';

describe('TeacherExamReportComponent', () => {
  let component: TeacherExamReportComponent;
  let fixture: ComponentFixture<TeacherExamReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeacherExamReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeacherExamReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
