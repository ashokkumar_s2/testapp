import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyschedulesComponent } from './myschedules.component';

describe('MyschedulesComponent', () => {
  let component: MyschedulesComponent;
  let fixture: ComponentFixture<MyschedulesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyschedulesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyschedulesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
