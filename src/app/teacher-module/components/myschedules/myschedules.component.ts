import { Component, OnInit, ViewChild } from '@angular/core';
import { TeacherService} from '../../../services/teacher.service';
import { Router } from '@angular/router';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { DataSource } from '@angular/cdk/table';

@Component({
  selector: 'app-myschedules',
  templateUrl: './myschedules.component.html',
  styleUrls: ['./myschedules.component.scss']
})
export class MyschedulesComponent implements OnInit {

  displayedColumns: string[] = ['Date', 'Class','Schedule', 'Session', 'Subject','Status'];
  dataSource :any;
  constructor(private teacherService:TeacherService,private router:Router) { }

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  ngOnInit() {
    this.teacherService.getAllSchedules().subscribe(res=>{
     this.dataSource = new MatTableDataSource(res.content);
     this.dataSource.paginator = this.paginator;
     this.dataSource.sort = this.sort;
    })
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
