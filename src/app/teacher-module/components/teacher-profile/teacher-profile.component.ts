import { Component, OnInit } from '@angular/core';
import { TeacherService } from '../../../services/teacher.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-teacher-profile',
  templateUrl: './teacher-profile.component.html',
  styleUrls: ['./teacher-profile.component.scss']
})
export class TeacherProfileComponent implements OnInit {

  userData:{};
  teacherSubjects:any[];
  studentId:any; 
  constructor(private teacherService:TeacherService,private route: ActivatedRoute) { }

  ngOnInit() {
    this.studentId = this.route.parent.snapshot.paramMap.get('id');
    this.teacherService.getProfileInfo(this.studentId).subscribe(res=>{
      console.log(res);
      this.userData = res.content[0][0];
      this.teacherSubjects = res.content[1];
      console.log(this.teacherSubjects);
    })
  }

}
