import { Component, OnInit } from '@angular/core';
import { TeacherService} from '../../../services/teacher.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-manage-students',
  templateUrl: './manage-students.component.html',
  styleUrls: ['./manage-students.component.scss']
})
export class ManageStudentsComponent implements OnInit {

  classInfo:[{}] = [{}]; 
  students:any[] = [];
  studentLoader: boolean = false;
  constructor(private teacherService:TeacherService,private router:Router) { }

  ngOnInit() {
    this.teacherService.getMyClassList().subscribe(res=>{
      this.classInfo = res.content;
      this.classInfo.forEach((section,index)=>{
       this.getStudentList(section['class_id'],index);
      })
    })
  }

  getStudentList(class_id,index:number){

    let data =[];
    this.teacherService.getListofStudents(class_id).subscribe(res=>{
      data = res.content;
      this.students[index] = data;
      if(index === this.classInfo.length-1){
        this.studentLoader = true
        console.log(this.students);
      }
      });
  }

}
