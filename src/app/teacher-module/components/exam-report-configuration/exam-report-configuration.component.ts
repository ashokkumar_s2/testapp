import { Component, OnInit } from '@angular/core';
import { TeacherService } from '../../../services/teacher.service';

@Component({
  selector: 'app-exam-report-configuration',
  templateUrl: './exam-report-configuration.component.html',
  styleUrls: ['./exam-report-configuration.component.scss']
})
export class ExamReportConfigurationComponent implements OnInit {

  configureList: any[] = []
  messagePanel: string = "";
  errorFlag: boolean = false;
  allExamSchedules: any[];
  sortedExamSchedules: any;
  upcomingExamSchedules: any[] = [];
  ongoingExamSchedules: any[] = [];
  completedExamSchedules: any[] = [];
  completedConfiguration: any[] = [];
  exams: any[] = [];
  today = new Date();

  constructor(private teacherService: TeacherService) { }

  ngOnInit() {
    this.teacherService.getPendingExamConf().subscribe(res => {
      if (res.status == 200) {
        this.configureList = res.content;
        console.log(this.configureList);
      }
      else {
        this.messagePanel = res.message;
        this.errorFlag = true;
      }
    })

    this.teacherService.getExamSchedule().subscribe(res => {
      this.allExamSchedules = res.content;
      this.sortedExamSchedules = this.groupBy(this.allExamSchedules, 'exam_id');
      console.log(this.sortedExamSchedules);
      for (var k in this.sortedExamSchedules) {
        this.exams.push(k)
      }
      this.exams.forEach(exam => {
        let lastIndex = this.sortedExamSchedules[exam].length - 1;
        console.log(this.sortedExamSchedules[exam][0].date);
        console.log(this.today.getTime());
        if ((new Date(this.sortedExamSchedules[exam][0].date)).getTime() <= this.today.getTime() && (new Date(this.sortedExamSchedules[exam][lastIndex].date)).getTime() >= this.today.getTime()) {
          let classwiseSchedule = this.groupBy(this.sortedExamSchedules[exam],'class_fk')
          for (var key in classwiseSchedule) {
            this.ongoingExamSchedules.push(classwiseSchedule[key]);
          }
        }
        else if (new Date(this.sortedExamSchedules[exam][0].date).getTime() >= this.today.getTime()) {
          this.upcomingExamSchedules.push(this.sortedExamSchedules[exam]);
        }
        else {
          // this.completedExamSchedules.push(this.sortedExamSchedules[exam]);
          let classwiseSchedule:any;
          classwiseSchedule = this.groupBy(this.sortedExamSchedules[exam],'class_fk')
          for (var key in classwiseSchedule) {
            let configurationFlag:boolean = true;
            console.log('classwise',classwiseSchedule[key]);
            classwiseSchedule[key].forEach(examSubject=>{
              if(examSubject.status_flag == null){
                configurationFlag = false;
              }
            })
            this.completedConfiguration.push({configurationFlag: configurationFlag})
            this.completedExamSchedules.push(classwiseSchedule[key]);
          }
        }
      })
      console.log('ongoingExamSchedules', this.ongoingExamSchedules);
      console.log('upcomingExamSchedules', this.upcomingExamSchedules);
      console.log('completedExamSchedules', this.completedExamSchedules);
    })
  }

  groupBy(xs, key) {
    return xs.reduce((rv, x) => {
      (rv[x[key]] = rv[x[key]] || []).push(x);
      return rv;
    }, {});
  };


  getDaysRemaining(date) {
    const diffTime = Math.abs(this.today.getDate() - new Date(date).getDate());
    const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
    return diffDays;
  }

}


