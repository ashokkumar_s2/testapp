import { Component, OnInit } from '@angular/core';
import { FormBuilder,FormGroup, Validators, FormArray } from '@angular/forms';
import { TeacherService} from '../../../services/teacher.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-create-schedule',
  templateUrl: './create-schedule.component.html',
  styleUrls: ['./create-schedule.component.scss']
})
export class CreateScheduleComponent implements OnInit {

  constructor(private formBuilder: FormBuilder, private teacherService:TeacherService, private route: ActivatedRoute) { }

  scheduleForm:FormGroup;
  submitted:boolean = false;
  classId: string;
  messagePanel:string;
  periodSplit:[{}] =[{}];
  listOfDays: [{}] = [{}];
  listOfPeriods: any[] = [];
  groupData:[{}] = [{}];
  groupOptions:[{}] = [{}];
  listOfGroups:[{}] =[{}];
  classSchedules:any[]=[];
  groupedSubjectInfo:[{}] = [{}];
  colorCodes:any[] = ["green","turquoise","navy","blue","purple","grey","red","orange","yellow","lime-green","saffron"]
  periodConfig = {
    labelField: 'period_name',
    valueField: 'period_id',
    plugins: [],
    searchField: ['period_id','period_name'],
    dropdownDirection: 'down'
  };
  dayConfig = {
    labelField: 'day_name',
    valueField: 'day_id',
    plugins: [],
    searchField: ['day_id','day_name'],
    dropdownDirection: 'down'
  };
  groupConfig = {
    labelField: 'group_name',
    valueField: 'group_id',
    plugins: [],
    searchField: ['group_name','group_id'],
    dropdownDirection: 'down'
  };

  ngOnInit() {
    this.classId = this.route.snapshot.paramMap.get('id');
    //service to get days
    this.teacherService.getListofDays().subscribe(res=>{
      this.listOfDays = res.content;
      // console.log(this.listOfDays);
    });

    //service to get periods
    this.teacherService.getListofPeriods().subscribe(res=>{
      this.listOfPeriods = res.content;
    })

    //service to get groups available to particular class
    this.teacherService.getGroupsofClass(this.classId).subscribe(res=>{
      this.groupData = res.content;
      this.groupOptions =  this.groupBy(this.groupData,'group_id');
      let groups = Object.keys(this.groupOptions);
      groups.forEach(group=>{
        let data={
          group_id:this.groupOptions[group][0].group_id,
          group_name: this.groupOptions[group][0].group_name,
          subjects: this.groupOptions[group]
        }
        this.listOfGroups.push(data);
      })
    })

    this.teacherService.getMyClass(this.classId).subscribe(res=>{
      if(res.status == 200){
        let subjectInfo = res.content;
        this.groupedSubjectInfo =   this.groupBy(subjectInfo,'group_id');
      }
    })

    this.teacherService.getSchedule({id:this.classId}).subscribe(res=>{
      let data = res.content;
      this.classSchedules = this.groupBy(data,'day_fk');
    })

    this.scheduleForm = this.formBuilder.group({
      day: ['',Validators.required],
      period:['',Validators.required],
      group: ['',Validators.required]
    })  
  }

  groupBy(xs, key) {
    return xs.reduce((rv, x)=> {
      (rv[x[key]] = rv[x[key]] || []).push(x);
      return rv;
    }, {});
  };

  get f() { return this.scheduleForm.controls; }

  submitFlag(){
    this.submitted = true;
  }
  
  onSubmit() {
    if(this.scheduleForm.valid){
        let data = this.scheduleForm.value;
        let changeId:boolean = false;
        data.class_id = this.classId;
        this.teacherService.addSchedule(data).subscribe(res=>{
          if(res.status === 200){

          if(this.classSchedules[data.day]){
            this.classSchedules[data.day].forEach(period=>{
              if(period.period_fk == data.period){
                period.group_fk = data.group;
                changeId = true;
              }
            })
          }
          else{
            this.classSchedules[data.day]=[];
          }

            if(!changeId){
              this.classSchedules[data.day].push({
                "group_fk": data.group,
                "class_fk": this.classId,
                "period_fk": data.period,
                "period_name": this.listOfPeriods.find(period=>period.period_id ==data.period).period_name
              })
              this.classSchedules[data.day].sort((a, b) => parseInt(a.period_fk) - parseInt(b.period_fk));
              // homes.sort((a, b) => parseFloat(a.price) - parseFloat(b.price));
            }

          }
          this.submitted = false;
          this.scheduleForm.reset();
        })
    // console.log(this.scheduleForm.value);
    }
    else{
     this.messagePanel = "Please fill the form with valid information";
    }
 }
} 
