import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { TeacherService } from '../../../services/teacher.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-config-detailed',
  templateUrl: './config-detailed.component.html',
  styleUrls: ['./config-detailed.component.scss']
})
export class ConfigDetailedComponent implements OnInit {



  constructor(private formBuilder: FormBuilder, private toastr: ToastrService, private route: ActivatedRoute, private teacherService: TeacherService) { }

  confExamForm: FormGroup;
  classId: any;
  examId: any;
  submitted: boolean = false;
  messagePanel: string;
  duplicateFlag: boolean = false;
  reportGroupOptions: any[] = []
  subjectList: any[] = [];
  freezeFormFlag: boolean = false;



  subjectConfig = {
    plugins: ['dropdown_direction', 'remove_button'],
    dropdownDirection: 'down',
    searchField: ['subject_id', 'subject_name'],
    labelField: 'subject_name',
    valueField: 'subject_id',
  }

  reportGroupConfig = {
    plugins: ['dropdown_direction', 'remove_button'],
    dropdownDirection: 'down',
    searchField: ['group_id', 'group_name'],
    labelField: 'group_name',
    valueField: 'group_id',
  }


  ngOnInit() {

    this.classId = this.route.snapshot.paramMap.get('id');
    this.examId = this.route.snapshot.paramMap.get('id2');

    this.teacherService.getExamConfDetail(this.classId, this.examId).subscribe(res => {
      if (res.status == 200) {
        this.subjectList = res.content;
        this.subjectList.forEach(subject => {
          this.reportGroupOptions.push({
            group_id: subject.group_id,
            group_name: subject.group_name
          })
        })
        this.setScheduleInfo();
      }
      else {
        //write dept failure fetch message
      }
    })
    this.confExamForm = this.formBuilder.group({
      standard: ['', [Validators.required]],
      examName: ['', [Validators.required]],
      scheduleInfo: this.formBuilder.array([])
    });

  }

  get f() { return this.confExamForm.controls; }

  setScheduleInfo() {
    this.confExamForm.controls.standard.setValue(this.subjectList[0].classname);
    this.confExamForm.controls.examName.setValue(this.subjectList[0].exam_name);
    let control = <FormArray>this.confExamForm.controls.scheduleInfo;
    this.subjectList.forEach(x => {
      control.push(this.formBuilder.group({
        id: [x.exam_schedule_id, Validators.required],
        subject: [x.subject_id, Validators.required],
        maxMarks: [x.max_marks, Validators.required],
        reportGroup: [x.group_id, Validators.required],
        initialGroup: [x.group_id, Validators.required],
        groupTotal: [x.max_marks, Validators.required],
        passMarks: [x.pass_marks, Validators.required]
      }))
    });
  }

  onSubmit() {
    
    this.submitted = true;
    console.log(this.confExamForm);
    if(this.confExamForm.valid){
      console.log("mapp");
      this.teacherService.postExamConfiguration(this.confExamForm.controls.scheduleInfo.value).subscribe(res=>{

      });
    }
  }

  reportGroupChange(initialGroup, reportGroup) {
    let control = <FormArray>this.confExamForm.controls.scheduleInfo;
    control.controls.forEach((uniqControl => {
      if (uniqControl["controls"].initialGroup.value == initialGroup && uniqControl["controls"].reportGroup.value != reportGroup) {
        uniqControl["controls"].reportGroup.setValue(reportGroup, {onlySelf: true, emitEvent: false});
      }
    }))

    let data: any[] = this.confExamForm.value.scheduleInfo;
    let groupedData = this.groupBy(data, "reportGroup");
    let passMarks = '';
    control.controls.forEach(uniqControl => {
      let tempValue: any[] = groupedData[uniqControl["controls"].reportGroup.value];
      console.log(tempValue);
      let groupTotal = 0
      let groupArr:any[] = [];
      //Add total of changed group to mapped group
      tempValue.forEach(data => {
        if(!(groupArr.includes(parseInt(data["initialGroup"])))){
          groupArr.push(parseInt(data["initialGroup"]));
          groupTotal = groupTotal + data["maxMarks"];
          console.log(groupTotal);
        }
      })
      uniqControl["controls"].groupTotal.setValue(groupTotal);
    })
    this.passMarksMap(reportGroup,null)
  }

  passMarksMap(reportGroup,passMarks){
    console.log("triggers");
    let control = <FormArray>this.confExamForm.controls.scheduleInfo;
    control.controls.forEach((uniqControl => {
      if (uniqControl["controls"].reportGroup.value == reportGroup) {
        uniqControl["controls"].passMarks.setValue(passMarks, {emitEvent: false});
      }
    }))
  }

  groupBy(xs, key) {
    return xs.reduce((rv, x) => {
      (rv[x[key]] = rv[x[key]] || []).push(x);
      return rv;
    }, {});
  };

}
