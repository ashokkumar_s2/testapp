import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectizeModule } from 'ng-selectize';
import { HttpClientModule } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { AuthGuard } from '../auth.guard';
import {MatTableModule} from '@angular/material/table';
import {MatSortModule} from '@angular/material/sort';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import { ChartsModule } from 'ng2-charts';

import { DashboardComponent } from './components/dashboard/dashboard.component';
import { StudentScannerComponent } from './components/student-scanner/student-scanner.component';
import { TeacherScannerComponent } from './components/teacher-scanner/teacher-scanner.component';
import { StudentDetailComponent } from './components/student-detail/student-detail.component';
import { AttendanceComponent } from './components/student/attendance/attendance.component';
import { ProfileComponent } from './components/student/profile/profile.component';
import { ExamreportsComponent } from './components/student/examreports/examreports.component';
import { ReportCardComponent } from './components/student/report-card/report-card.component';

const routes: Routes = [
  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard], data: { roles: [5] } },
  { path: 'studentscanner', component: StudentScannerComponent, canActivate: [AuthGuard], data: { roles: [5] } },
  { path: 'teacherscanner', component: TeacherScannerComponent, canActivate: [AuthGuard], data: { roles: [5] } },
  {
    path: 'studentdetail/:id',
    component: StudentDetailComponent,
    children: [
      { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard], data: { roles: [5] } },
      { path: 'attendance', component: AttendanceComponent, canActivate: [AuthGuard], data: { roles: [5] } },
      { path: 'examreport', component: ExamreportsComponent, canActivate: [AuthGuard], data: { roles: [5] } },
      { path: 'studentreport/:id2', component: ReportCardComponent, canActivate: [AuthGuard], data: { roles: [5] } },
      { path: '', redirectTo: 'profile', pathMatch: 'full' }
    ]
  },
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' }
];

@NgModule({
  declarations: [
    DashboardComponent,
    StudentScannerComponent,
    TeacherScannerComponent,
    StudentDetailComponent,
    AttendanceComponent,
    ProfileComponent,
    ExamreportsComponent,
    ReportCardComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    InfiniteScrollModule,
    MatTableModule,
    MatFormFieldModule,
    MatSortModule,
    MatInputModule, 
    MatPaginatorModule,
    NgSelectizeModule,
    HttpClientModule,
    ChartsModule,
    ToastrModule.forRoot({
      timeOut: 1000,
      positionClass: 'toast-center-center'
    }),
    RouterModule.forChild(routes)
  ]
})


export class ManagementModuleModule { }
