import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeacherScannerComponent } from './teacher-scanner.component';

describe('TeacherScannerComponent', () => {
  let component: TeacherScannerComponent;
  let fixture: ComponentFixture<TeacherScannerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeacherScannerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeacherScannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
