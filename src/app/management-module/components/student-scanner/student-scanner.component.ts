import { Component, OnInit } from '@angular/core';
import {ManagementService} from "../../../services/management.service";

@Component({
  selector: 'app-student-scanner',
  templateUrl: './student-scanner.component.html',
  styleUrls: ['./student-scanner.component.scss']
})
export class StudentScannerComponent implements OnInit {

  constructor(private managementService:ManagementService) { }

  searchText:string;
  scrollIndex:number = 0;
  lockScroll:boolean = false;
  matchedData:any[]=[];

  ngOnInit() {
  }

  fetchResult(){
    this.managementService.getScannedStudents(this.searchText,12).subscribe(res=>{
      this.matchedData = res.content;
      this.scrollIndex = 1;
    })
  }

  onScroll() {
    this.scrollIndex = this.scrollIndex+1;
    this.managementService.getScannedStudents(this.searchText,(this.scrollIndex*12)).subscribe(res=>{
      if (res.status == 200) {
          this.matchedData = this.matchedData.concat(res.content);
          console.log(this.matchedData);
      }
      else if (res.content.length === 0){
          this.lockScroll = true;
      }
      else{
        //error flag
      }
    });
  }

}
