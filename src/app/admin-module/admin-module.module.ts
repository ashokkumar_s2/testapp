import { NgModule } from '@angular/core'; 
import { CommonModule } from '@angular/common';
import { NgbDatepickerModule} from '@ng-bootstrap/ng-bootstrap';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectizeModule } from 'ng-selectize';
import { HttpClientModule } from '@angular/common/http';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { ToastrModule } from 'ngx-toastr';

import { CreateClassComponent } from './components/create-class/create-class.component';
import { CreateTeachersComponent } from './components/create-teachers/create-teachers.component';
import { MiscellaneousFormsComponent} from './components/miscellaneous-forms/miscellaneous-forms.component';
import { ExamSchedulerComponent } from './components/exam-scheduler/exam-scheduler.component';
import { DaySchedulerComponent } from './components/day-scheduler/day-scheduler.component';
import { SubjectActivitiesComponent } from './components/subject-activities/subject-activities.component';
import { AuthGuard } from '../auth.guard';
import { DashboardComponent } from './components/dashboard/dashboard.component';

const routes: Routes = [
  { path: 'dashboard' , component: DashboardComponent,canActivate:[AuthGuard], data: { roles: [2] }},
  { path: 'createClass', component: CreateClassComponent,canActivate:[AuthGuard], data: { roles: [2] }},
  { path: 'miscForms', component: MiscellaneousFormsComponent,canActivate:[AuthGuard], data: { roles: [2] }},
  { path: 'createExams', component: ExamSchedulerComponent,canActivate:[AuthGuard],data: { roles: [2] }},
  { path: 'subjectActivity', component: SubjectActivitiesComponent,canActivate:[AuthGuard],data: { roles: [2] }},
  { path: 'dayscheduler', component: DaySchedulerComponent ,canActivate:[AuthGuard],data: { roles: [2] }},
  { path: 'createTeachers', component: CreateTeachersComponent,canActivate:[AuthGuard],data: { roles: [2] }},
  { path: '', redirectTo:'dashboard',pathMatch:'full' }
];

@NgModule({
  declarations: [
    CreateClassComponent,
    CreateTeachersComponent,
    MiscellaneousFormsComponent,
    SubjectActivitiesComponent,
    DaySchedulerComponent,
    ExamSchedulerComponent,
    DashboardComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectizeModule,
    NgbModule,
    NgbDatepickerModule,
    HttpClientModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    ToastrModule.forRoot({
      timeOut: 1000,
      positionClass: 'toast-center-center'
    }),
    RouterModule.forChild(routes) 
  ]
})
export class AdminModuleModule { }
