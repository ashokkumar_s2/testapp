import { Component, OnInit } from '@angular/core';
import { FormBuilder,FormGroup,FormArray, Validators } from '@angular/forms';
import { AdminFormsService } from '../../../services/admin-forms.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-exam-scheduler',
  templateUrl: './exam-scheduler.component.html',
  styleUrls: ['./exam-scheduler.component.scss']
})
export class ExamSchedulerComponent implements OnInit {

  constructor(private formBuilder: FormBuilder,private toastr: ToastrService, private adminForm: AdminFormsService) { }

  createExamForm:FormGroup;
  submitted:boolean = false;
  messagePanel:string;
  duplicateFlag:boolean = false;
  standardOptions:any[] = [];
  examOptions:any[] = [];
  sectionOptions:any[] = [];
  sessionOptions:any[] = [];
  subjectList:any[] = [];
  freezeFormFlag:boolean = false;

  sectionConfig = {
    highlight: false,
    create: false,
    persist: true,
    plugins: ['dropdown_direction', 'remove_button'],
    dropdownDirection: 'down',
    labelField: 'section_name',
	  valueField: 'class_id',
  	maxItems: 24
  }

  standardConfig = {
    plugins: ['dropdown_direction', 'remove_button'],
    dropdownDirection: 'down',
    searchField: ['std_name', 'std_id'],
    labelField: 'std_name',
	  valueField: 'std_id',
  }

  examConfig = {
    plugins: ['dropdown_direction', 'remove_button'],
    dropdownDirection: 'down',
    searchField: ['exam_id', 'exam_name'],
    labelField: 'exam_name',
	  valueField: 'exam_id',
  }
  
  subjectConfig = {
    plugins: ['dropdown_direction', 'remove_button'],
    dropdownDirection: 'down',
    searchField: ['subject_fk', 'subject_name'],
    labelField: 'subject_name',
	  valueField: 'subject_fk',
  }

  sessionConfig = {
    plugins: ['dropdown_direction', 'remove_button'],
    dropdownDirection: 'down',
    searchField: ['period_id', 'period_name'],
    labelField: 'period_name',
	  valueField: 'period_id',
  }


  ngOnInit() {
   
    this.adminForm.getStandards().subscribe(res=>{
      if(res.status == 200){
        this.standardOptions = res.content;
      }
      else{
        //write dept failure fetch message
      }
    })

    this.adminForm.getExamNames().subscribe(res=>{
      if(res.status == 200){
        this.examOptions = res.content;
      }
      else{
        //write dept failure fetch message
      }
    })

    this.createExamForm = this.formBuilder.group({
      standard: ['',[Validators.required]],
      examName: ['',[Validators.required]],
      selectedSections: ['',[Validators.required]],
      scheduleInfo: this.formBuilder.array([])
    });

  }

  get f() { return this.createExamForm.controls; }

  setScheduleInfo() {
    let control = <FormArray>this.createExamForm.controls.scheduleInfo;
    this.subjectList.forEach(x =>{
        control.push(this.formBuilder.group({
          subject:[x.subject_fk, Validators.required],
          date:['', Validators.required],
          session:['', Validators.required],
          maxMarks:['', Validators.required],
          syllabus:['', Validators.required]
        }))
      });
      console.log(this.f);
    }

  onSubmit(){
    console.log("triggeres");
    this.submitted = true;
    if(this.createExamForm.valid && !this.duplicateFlag){
     console.log(this.createExamForm.value);
     this.adminForm.insertExamSchedules(this.createExamForm.value).subscribe(res=>{
      if(res.status == 200){
        this.toastr.info('Hoorayy all set! ',res.message);
      }
      else{
        this.toastr.warning('Contact Admin : ',res.message);
      }
     })
    }
   
  }

  duplicateSubjectCheck(){
    console.log("triggered");
    let tempData = this.createExamForm.value;
    console.log(tempData);
    let valueArr = tempData.scheduleInfo.map((item)=>{ return parseInt(item.subject) });
    let isDuplicate = valueArr.some((item, value)=>{ 
        return valueArr.indexOf(item) != value 
    });
    if(isDuplicate){
      this.duplicateFlag = true;
    }
    else{
      this.duplicateFlag = false;
    }
  }

  fetchSections(){
    this.adminForm.getListofSections(this.f.standard.value).subscribe(res=>{
      if(res.status == 200){
        this.sectionOptions = res.content;
        console.log(this.sectionOptions);
      }
      else{
        //write dept failure fetch message
      }
    })
  }

  fetchSubjectInfo(){
    this.adminForm.getSessions().subscribe(res=>{
      if(res.status == 200){
        this.sessionOptions = res.content;
        console.log(this.sessionOptions);
      }
      else{
        //write dept failure fetch message
      }
    })
    
    this.adminForm.getListofSubjects(this.f.selectedSections.value).subscribe(res=>{
      if(res.status == 200){
        this.freezeFormFlag = true;
        this.subjectList = res.content;
        console.log(this.subjectList);
        this.setScheduleInfo();
        console.log(this.subjectList);
      }
      else{
        //write dept failure fetch message
      }
    })
  }


}
