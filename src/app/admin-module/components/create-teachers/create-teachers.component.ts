import { Component, OnInit } from '@angular/core';
import { FormBuilder,FormGroup, Validators } from '@angular/forms';
import { AdminFormsService } from '../../../services/admin-forms.service';

@Component({
  selector: 'app-create-teachers',
  templateUrl: './create-teachers.component.html',
  styleUrls: ['./create-teachers.component.scss']
})
export class CreateTeachersComponent implements OnInit {

  constructor(private formBuilder: FormBuilder, private adminForm: AdminFormsService) { }
  
  createTeacherForm:FormGroup;
  submitted:boolean = false;
  emailPattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  phonePattern = /^[6789]\d{9}$/;
  userNamePattern =/^[a-z][a-z0-9]{6,15}$/;
  passwordPattern = /(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/
  departmentOptions: any = [];
  roleOptions:any=[];
  messagePanel:string;

  selectConfig = {
    labelField: 'dept_name',
    valueField: 'dept_id',
    plugins: [],
    searchField: ['dept_name', 'dept_id'],
    dropdownDirection: 'down'
  };



  ngOnInit() {

    this.adminForm.getDepartments().subscribe(res=>{
      if(res.status == 200){
        this.departmentOptions = res.content;
      }
      else{
        //write dept failure fetch message
      }
    })

    this.adminForm.getRoles().subscribe(res=>{
      if(res.status == 200){
        this.roleOptions = res.content;
        console.log(this.roleOptions);
      }
      else{

      }
    })

    this.createTeacherForm = this.formBuilder.group({
      firstName: ['',[Validators.required,Validators.minLength(3)]],
      lastName: ['',[Validators.required,Validators.minLength(3)]],
      gender: [0,Validators.required],
      dob:['',Validators.required],
      qualification:['',Validators.required],
      department:['',Validators.required],
      grade:['',Validators.required],
      joindate:['',Validators.required],
      mail:['',[Validators.required, Validators.pattern(this.emailPattern)]],
      password:['',[Validators.required, Validators.pattern(this.passwordPattern)]],
      userName:['',[Validators.required, Validators.pattern(this.userNamePattern)]],
      primaryContact:['',[Validators.required, Validators.pattern(this.phonePattern)]],
      secondaryContact:['',[Validators.required, Validators.pattern(this.phonePattern)]],
      address1:['',[Validators.required,Validators.minLength(3)]],
      address2:['',[Validators.required,Validators.minLength(3)]],
      city:['',[Validators.required,Validators.minLength(3)]],
      postcode:['',Validators.required],
      state:['',[Validators.required,Validators.minLength(3)]]
    });
  }

  get f() { return this.createTeacherForm.controls; }

  submitFlag(){
    this.submitted = true;
  }

  onSubmit() {
   if(this.createTeacherForm.valid){
    this.adminForm.postTeachers(this.createTeacherForm.value).subscribe(res=>{
      console.log(res);
    })
   }
   else{
    this.messagePanel = "Please fill the form with valid information";
   }
}

}
